Firefox 52 [ESR](https://www.mozilla.org/en-US/firefox/organizations/), packaged as an RPM for Fedora.  Dowload from [Fedora Copr](https://copr.fedorainfracloud.org/coprs/mavit/firefox-esr/).
